package tb.sockets.server;

import java.io.*;
import java.net.*;

import tb.sockets.server.MainFrame;

public class Server 
{
	public static  int pozycja;
	public static String wysl="none" , received="none";
	public String popWysl="none" , popReciv ="none";
	static boolean tura = true;
	ServerSocket sersock;
	Socket sock;
	OutputStream os;
	PrintWriter prwrite;
	InputStream is;
	BufferedReader receiveReadbuff;
	public Server() throws Exception
	{
		sersock = new ServerSocket(6666);
	    sock = sersock.accept();                          
	    os = sock.getOutputStream(); 
	    prwrite = new PrintWriter(os, true);
	    is = sock.getInputStream();
	    receiveReadbuff = new BufferedReader(new InputStreamReader(is));
	}
	
	public void Play() throws Exception
	{               
	    while(true)
	    {
	    	prwrite.flush();
	    	if(tura == true)
	    	{
	    		if(pozycja==0 )// serwer na poczatku wysyla prawidlowe haslo do klienta, do ktorego beda przyrownywane zgadywane wartosci
	    		{
	    			prwrite.println(MainFrame.frame.odp);
	    			pozycja++;
	    			prwrite.flush();
	    			MainFrame.frame.lblNotConnected.setText("Connected!"); // jesli haslo zostanie przeslane to znaczy ze polaczenie jest nawiazane
	    			tura = false;
	    		    char[] temp = new char[MainFrame.frame.odp.length()];	    			
	    		    for(int i=0; i<MainFrame.frame.odp.length(); i++)
	    			{	    				
	    			 	temp[i]='-';
	    			}
	    		    String temp2= new String(temp);
	    		    MainFrame.frame.ltr.setText(temp2);
	    			MainFrame.frame.lbltura.setText("Tura przeciwnika");
	    		}
	    		if(wysl!= popWysl&&wysl.length()==1)
	    		{
	    			prwrite.println(wysl);
	    			wysl= popWysl;
	    			prwrite.flush();
	    			tura = false;
	    			MainFrame.frame.lbltura.setText("Tura przeciwnika");

	    		}
	    		if(wysl!= popWysl&&wysl.length()>1)
	    		{
	    			prwrite.println(wysl);
	    			wysl= popWysl;
	    			prwrite.flush();
	    			tura = false;
	    			MainFrame.frame.lbltura.setText("Tura przeciwnika");


	    		}
	    	}
	    	
	    	if(tura == false)
	    	{
	    		received = receiveReadbuff.readLine();
				if(received != popReciv&&received.length()==1)
				{
					MainFrame.frame.literaPrzych=received.charAt(0);
					MainFrame.frame.SprawdzeniePrzych();
					popReciv = received;
					tura = true;
					MainFrame.frame.lbltura.setText("Twoja tura");
				}
				if(received != popReciv&&received.length()>1)
				{
					MainFrame.frame.hasloprzes=received;
					MainFrame.frame.SprawdzenieHPrzyh();
					popReciv = received;
					tura = true;
					MainFrame.frame.lbltura.setText("Twoja tura");
				}
	    	}

	    }
	}
	@Override
	protected void finalize() throws IOException
	{
		 sock.close();
		 os.close();
		 prwrite.close();
		 is.close();
		 receiveReadbuff.close();
	}
}