package tb.sockets.server;


import java.awt.EventQueue;
import java.awt.Font;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import tb.sockets.server.Server;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.border.LineBorder;

public class MainFrame extends JFrame  {

	private JPanel contentPane;
	public  String odp, haslo, hasloprzes;
	public  char litera, literaPrzych;
	public  OrderPane panel;
	public  JButton btnlit, btnhas ;
	public  JTextField text, textanswer;
	private  boolean jest=false;
	 JLabel ltr= new JLabel(" ", SwingConstants.CENTER);
	 JLabel lblNotConnected, lbltura;
	static MainFrame frame;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {

					 frame = new MainFrame();
					frame.setVisible(true);
					JOptionPane.showMessageDialog(null, "Zasady gry: nalezy odgadnac haslo(wpisywane w pole Word)."+System.lineSeparator()+"Ulatwieniem jest odkrywanie liter znajdujacym sie w hasle (wpisywane w pole Letter)"+System.lineSeparator()+"Za kazdy blad (litere badz haslo) dodawana jest czesc wisielca (jest ich 10)"+System.lineSeparator()+"Powodzenia!");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		
		new Thread() {
			public void run() {
				try {
					Server server = new Server();
					server.Play();
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}.start();
	}

	/**
	 * Create the frame.
	 * @throws IOException 
	 */
	public MainFrame() throws IOException {
		String[] tablica = {"ekshumacja", "mezopotamia", "wykroczenie","brawura","bezpieczny"}; //baza danych z ktorej losowana jest odpowiedz, skromna do celow testowych
		int rzut = (int) (Math.random()*5);
		odp=String.valueOf(tablica[rzut]);
		

		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds((screen.width-622)/2, (screen.height-520)/2, 622, 520);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setTitle("Serwer");
		
		panel = new OrderPane();
		panel.setBounds(145, 14, 450, 450);
		contentPane.add(panel);
		
		lblNotConnected = new JLabel("Not Connected", SwingConstants.CENTER);
		lblNotConnected.setForeground(new Color(255, 255, 255));
		lblNotConnected.setBackground(new Color(128, 128, 128));
		lblNotConnected.setOpaque(true);
		lblNotConnected.setBounds(10, 104, 123, 23);
		contentPane.add(lblNotConnected);
		lbltura = new JLabel("Twoja tura", SwingConstants.CENTER);
		lbltura.setBounds(10, 80, 123, 23);
		lbltura.setForeground( Color.WHITE);
		lbltura.setBackground(Color.BLACK);
		lbltura.setOpaque(true);
		contentPane.add(lbltura);

		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new LineBorder(new Color(0, 0, 0), 3, true));
		panel_1.setBounds(10, 138, 123, 200);
		contentPane.add(panel_1);
		panel_1.setLayout(new GridLayout(5, 1, 5, 5));
		text= new JTextField("Letter") ;
		panel_1.add(text);
		btnlit = new JButton("Send");
		btnlit.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				if(Server.tura == true)
				{
					if(text.getText().length()==1 &&ltr.getText().indexOf(text.getText().charAt(0))<0&&panel.Zlelitery.getText().indexOf(text.getText().charAt(0))<0)
					{
					litera=text.getText().charAt(0);
					Server.wysl=text.getText();
					Sprawdzenie();	
					}
					else
					{
						JOptionPane.showMessageDialog(null, "Zla wartosc zostala wpisana!");
					}
				}
			}
		});
		panel_1.add(btnlit);
		textanswer= new JTextField("Word");
		panel_1.add(textanswer);
		btnhas = new JButton("Send");
		btnhas.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				if(Server.tura == true)
				{
					if(textanswer.getText().length()>1 &&panel.Zlehasla.getText().indexOf(textanswer.getText())<0)
					{
					haslo=textanswer.getText();
					Server.wysl=textanswer.getText();
					SprawdzenieH();	
					}
					else
					{
						JOptionPane.showMessageDialog(null, "Zla wartosc zostala wpisana!");
					}
				}
			}
		});
		panel_1.add(btnhas);
		panel_1.add(ltr);
		ltr.setFont(new Font("Century Gothic", Font.BOLD, 14 ));
	}
private void Sprawdzenie()
{
    char[] temp = ltr.getText().toCharArray();    			
	for(int i=0; i<odp.length(); i++)
	{
		if(litera==odp.charAt(i))
		{
			temp[i]=litera;
			jest=true;
		}
	}
    String temp2= new String(temp);
    ltr.setText(temp2);
    if(jest==false)
    {
		panel.numer++;
		panel.repaint();
		panel.Zlelitery.setText(panel.Zlelitery.getText()+" "+litera);
    }
    jest=false;
    if(panel.numer==10||ltr.getText().compareTo(odp)==0)
    {
		JOptionPane.showMessageDialog(null, "Remis");
		frame.dispose();
    }
}
public  void SprawdzeniePrzych()
{
    char[] temp = ltr.getText().toCharArray();    			
	for(int i=0; i<odp.length(); i++)
	{
		if(literaPrzych==odp.charAt(i))
		{
			temp[i]=literaPrzych;
			jest=true;
		}
	}
    String temp2= new String(temp);
    ltr.setText(temp2);
    if(jest==false)
    {
		panel.numer++;
		panel.repaint();
		panel.Zlelitery.setText(panel.Zlelitery.getText()+" "+literaPrzych);
    }
    jest=false;
    if(panel.numer==10||ltr.getText().compareTo(odp)==0)
    {
		JOptionPane.showMessageDialog(null, "Remis");
		frame.dispose();
		System.exit(getDefaultCloseOperation());
    }
}
public  void SprawdzenieH()
{

    if(odp.compareTo(haslo)!=0)
    {
		panel.numer++;
		panel.repaint();
		panel.Zlehasla.setText(panel.Zlehasla.getText()+System.lineSeparator()+haslo);
	    if(panel.numer==10||ltr.getText().compareTo(odp)==0)
	    {
			JOptionPane.showMessageDialog(null, "Remis");
			frame.dispose();
			System.exit(getDefaultCloseOperation());
	    }
    }
    else
    {
    	ltr.setText(haslo);
		JOptionPane.showMessageDialog(null, "Serwer wygrana!");
		frame.dispose();
		System.exit(getDefaultCloseOperation());
    }
}
public  void SprawdzenieHPrzyh()
{

    if(odp.compareTo(hasloprzes)!=0)
    {
		panel.numer++;
		panel.repaint();
		panel.Zlehasla.setText(panel.Zlehasla.getText()+System.lineSeparator()+hasloprzes);
	    if(panel.numer==10||ltr.getText().compareTo(odp)==0)
	    {
			JOptionPane.showMessageDialog(null, "Remis");
			frame.dispose();
			System.exit(getDefaultCloseOperation());
	    }
    }
    else
    {
    	ltr.setText(hasloprzes);
		JOptionPane.showMessageDialog(null, "Serwer przegrana!");
			frame.dispose();
			System.exit(getDefaultCloseOperation());
}
}

}
