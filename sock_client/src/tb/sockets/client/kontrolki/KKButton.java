/**
 * 
 */
package tb.sockets.client.kontrolki;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.IndexColorModel;

import javax.swing.JButton;

/**
 * @author tb
 *
 */
@SuppressWarnings("serial")
public class KKButton extends JButton {

	private BufferedImage[] rysunki = new BufferedImage[3];
	private int stan = 0;
	
	public KKButton() {
		super("");
		for (int i=0;i<3; i++) {
			IndexColorModel kolor=(IndexColorModel) createColorModel(2);
			BufferedImage tI = new BufferedImage(50, 50, BufferedImage.TYPE_BYTE_INDEXED, kolor);
			tI.getGraphics().setColor(Color.YELLOW);
			tI.getGraphics().drawOval(4, 4,20, 20);
			tI.getGraphics().fillOval(4, 4,20, 20);
			rysunki[i] = tI;
		}
		
	}
	/* (non-Javadoc)
	 * @see javax.swing.JComponent#paintComponent(java.awt.Graphics)
	 */
	@Override
	protected void paintComponent(Graphics g) {
		// TODO Auto-generated method stub
		super.paintComponent(g);
		g.drawImage(rysunki[stan], 0, 0, null);
	}
	private static  IndexColorModel createColorModel(int n) {
	    // Create a simple color model with all values mapping to
	    // a single shade of gray
	    // nb. this could be improved by reusing the byte arrays

	    byte[] r = new byte[16];
	    byte[] g = new byte[16];
	    byte[] b = new byte[16];
	    for (int i = 0; i < r.length; i++) {
	        r[i] = (byte) n;
	        g[i] = (byte) n;
	        b[i] = (byte) n;
	    }
	    IndexColorModel u=new IndexColorModel(4, 16, r, g, b);
	    return u;
	}
}
