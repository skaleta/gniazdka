package tb.sockets.client;

import javax.swing.JPanel;
import javax.swing.JTextArea;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Area;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Path2D;


public class OrderPane extends JPanel 
	{
int numer;
JTextArea Zlelitery, Zlehasla;
		public OrderPane()
		{
			super();
			Zlelitery= new JTextArea("");// w tym miejscu wypisywane sa litery ktorych nie ma w hasle
			Zlelitery.setEditable(false);

	        add(Zlelitery);
	        Zlehasla= new JTextArea("");// w tym miejscu wypisywane sa zle hasla
	        Zlehasla.setEditable(false);

	        add(Zlehasla);
			setBackground( Color.WHITE);
		}
		public  void rys(Graphics g) // funkcja malujaca czesci wisielca, zmienna numer pokazuje ile ich ma byc
		{
	        Graphics2D g2d = (Graphics2D) g;
				if(numer>=1)
				{		
				Path2D myPath = new Path2D.Double();
			    myPath.moveTo(0, 430);
			    myPath.lineTo(430, 430);
			    myPath.closePath();
			    g2d.draw(myPath);
				}
				if(numer>=2)
				{		
				Path2D myPath = new Path2D.Double();
			    myPath.moveTo(50, 430);
			    myPath.lineTo(50, 10);
			    myPath.closePath();
			    g2d.draw(myPath);
				}	
				if(numer>=3)
				{		
				Path2D myPath = new Path2D.Double();
			    myPath.moveTo(50, 10);
			    myPath.lineTo(100, 10);
			    myPath.closePath();
			    g2d.draw(myPath);
				}
				if(numer>=4)
				{			
				Path2D myPath = new Path2D.Double();
			    myPath.moveTo(100, 10);
			    myPath.lineTo(100, 30);
			    myPath.closePath();
			    g2d.draw(myPath);
				}
				if(numer>=5)
				{			

				Ellipse2D myell = new Ellipse2D.Double(90,30,20,20);
			    Area area= new Area(myell);	
			    g2d.draw(area);
				}	
				if(numer>=6)
				{			
				Path2D myPath = new Path2D.Double();
			    myPath.moveTo(100, 50);
			    myPath.lineTo(100, 80);
			    myPath.closePath();
			    g2d.draw(myPath);			
			    }
				if(numer>=7)
				{			
				Path2D myPath = new Path2D.Double();
			    myPath.moveTo(100, 50);
			    myPath.lineTo(85, 65);
			    myPath.closePath();
			    g2d.draw(myPath);		
			    }
				if(numer>=8)
				{			
				Path2D myPath = new Path2D.Double();
			    myPath.moveTo(100, 50);
			    myPath.lineTo(115, 65);
			    myPath.closePath();
			    g2d.draw(myPath);
				}
				if(numer>=9)
				{			
				Path2D myPath = new Path2D.Double();
			    myPath.moveTo(100, 80);
			    myPath.lineTo(85, 115);
			    myPath.closePath();
			    g2d.draw(myPath);
				}
				if(numer>=10)
				{			
				Path2D myPath = new Path2D.Double();
			    myPath.moveTo(100, 80);
			    myPath.lineTo(115, 115);
			    myPath.closePath();
			    g2d.draw(myPath);
				}
				Zlelitery.setBounds(350,10,200,100);

				Zlehasla.setBounds(350,150,200,100);

		}



			@Override
			protected void paintComponent(Graphics g) {
				super.paintComponent(g); //Override tej funkcji sprawia ze wisielec jest przerysowywany za kazdym razem gdy zostanie wywolana funkcja repaint
					rys(g);

			}

	}
