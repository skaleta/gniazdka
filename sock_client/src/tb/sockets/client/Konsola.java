package tb.sockets.client;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

public class Konsola {

	public static void main(String[] args) {
		try {
			Socket sock = new Socket("192.168.1.1", 6666);
			DataInputStream in = new DataInputStream(sock.getInputStream());
			BufferedReader is = new BufferedReader(new InputStreamReader(in));
			System.out.println("Przeczytano z gniazdka: " + is.readLine());
			is.close();
			in.close();	
			sock.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
