package tb.sockets.client;

import java.io.*;
import java.net.*;

import tb.sockets.client.MainFrame;

public class Client 
{
	public static  int pozycja;
	public static String wysl="none" , received="none";
	public String popWysl="none" , popReciv ="none";
	static boolean tura = false;
	Socket sock;
	OutputStream os;
	PrintWriter prwrite;
	InputStream is;
	BufferedReader receiveReadbuff;
	public Client(String host, int port) throws Exception
	{
		sock = new Socket(host, port);
		os = sock.getOutputStream(); //outputstream klienta
		prwrite = new PrintWriter(os, true); // sluzy do wysylania zmiennych
		is = sock.getInputStream();// inputstream klienta
		receiveReadbuff = new BufferedReader(new InputStreamReader(is)); // sluzy do oddczytywania otwrzyamnych zmiennych

	}
	 
	public void Play() throws Exception
	{        
		while(true)
		{
			prwrite.flush();
			if(tura == true)// gdy gracz ma swoja ture to moze wysylac informacje
			{

	    		if(wysl!= popWysl&&wysl.length()==1) //wysylanie pojedynczej litery
	    		{	
	    			prwrite.println(wysl);
	    			wysl= popWysl;
	    			prwrite.flush();
	    			tura = false;
	    			MainFrame.frame.lbltura.setText("Tura przeciwnika");

	    		}
	    		if(wysl!= popWysl&&wysl.length()>1) //wysylanie calego hasla
	    		{
	    			prwrite.println(wysl);
	    			wysl= popWysl;
	    			prwrite.flush();
	    			tura = false;
	    			MainFrame.frame.lbltura.setText("Tura przeciwnika");// gdy opdiwiedz zotanie wyslana konczy sie tura danego gracza

	    		}
			}
			if(tura == false)// gdy gracz nie ma swojej tury to czeka na odp przeciwnika
			{	
				received = receiveReadbuff.readLine();
				if(pozycja==0)//pierwsza rzecza ktora wysyla klient jest wlasciwa odpowiedz, dzieki czemu funkcje sprawdzajace odpowiedzi wysylane przez uzytkownika dzialaja
				{
					tura = true;
					MainFrame.frame.odp= String.valueOf(received);
	    		    char[] temp = new char[MainFrame.frame.odp.length()];	    			
	    		    for(int i=0; i<MainFrame.frame.odp.length(); i++)
	    			{	    				
	    			 	temp[i]='-';
	    			}
	    		    String temp2= new String(temp); //tworzenie miejsca na odpowiedz na panelu, gdy zadne litery nie sa odkryte to ma postac "--------..."
	    		    MainFrame.frame.ltr.setText(temp2);
	    		    pozycja++;
	    		    received="none";
	    		    MainFrame.frame.lbltura.setText("Twoja tura");
				}

				if(received != popReciv&&received.length()==1)// odbieranie pojedynczej litery
				{
					MainFrame.frame.literaPrzych=received.charAt(0);
					MainFrame.frame.SprawdzeniePrzych();
					popReciv = received;
					tura = true;
					MainFrame.frame.lbltura.setText("Twoja tura");// gdy uzytkownik uzyska odpowiedz od przeciwnika to zaczyna sie jego tura
				}
				if(received != popReciv&&received.length()>1)// odbieranie calego potencjalnego hasla
				{// warunek received != popReciv sprawdza czy wysylana jest nowa odpowiedz
					MainFrame.frame.hasloprzes=received;
					MainFrame.frame.SprawdzenieHPrzyh();
					popReciv = received;
					tura = true;
					MainFrame.frame.lbltura.setText("Twoja tura");
				}
			}
			
		}
	}
	@Override //dla pewnosci zamykanie wszystkiego
	protected void finalize() throws IOException
	{
		 sock.close();
		 os.close();
		 prwrite.close();
		 is.close();
		 receiveReadbuff.close();
	}
}