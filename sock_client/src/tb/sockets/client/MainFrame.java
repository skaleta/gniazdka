package tb.sockets.client;

import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import java.awt.Font;
import tb.sockets.client.OrderPane;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.text.ParseException;
import javax.swing.border.LineBorder;
import javax.swing.text.MaskFormatter;

public class MainFrame extends JFrame {
	public  String odp, haslo, hasloprzes;
	public  char litera, literaPrzych;
	private  boolean jest=false;
	private JPanel contentPane;
	public  OrderPane panel;
	public  JButton btnlit, btnhas ;
	public  JTextField text, textanswer;
	 JLabel ltr= new JLabel(" ", SwingConstants.CENTER);
	 JLabel lblNotConnected, lbltura;
	static MainFrame frame; //Ramka w ktorej ma dzialac cala aplikacja jest statyczna
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					 frame = new MainFrame();
					frame.setVisible(true);
					JOptionPane.showMessageDialog(null, "Zasady gry: nalezy odgadnac haslo(wpisywane w pole Word)."+System.lineSeparator()+"Ulatwieniem jest odkrywanie liter znajdujacym sie w hasle (wpisywane w pole Letter)"+System.lineSeparator()+"Za kazdy blad (litere badz haslo) dodawana jest czesc wisielca (jest ich 10)"+System.lineSeparator()+"Powodzenia!");

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

	}

	/**
	 * Create the frame.
	 * @throws IOException 
	 */
	public MainFrame() throws IOException {
		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds((screen.width-622)/2, (screen.height-520)/2, 622, 520);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setTitle("Klient");
		JLabel lblHost = new JLabel("Host:");
		lblHost.setBounds(10, 14, 30, 14);
		contentPane.add(lblHost);
		
		 JFormattedTextField frmtdtxtfldIp = null ;
		try {
	
			MaskFormatter mf = new MaskFormatter("###.###.###.###"); 
			mf.setPlaceholder("");
			JFormattedTextField fText = new JFormattedTextField(mf);
			fText.setFocusLostBehavior(JFormattedTextField.PERSIST);
			frmtdtxtfldIp = new JFormattedTextField(fText); //Takie przypisanie MaskFormattera sprawia ze wpisany moze byc kazdy adres, a nie tylko ten w domyslnym formacie
			frmtdtxtfldIp.setBounds(43, 11, 90, 20); 
			frmtdtxtfldIp.setText("xxx.xxx.xxx.xxx");
			contentPane.add(frmtdtxtfldIp);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		JFormattedTextField tempo= frmtdtxtfldIp; //Deklaracja konieczna by miec dostep do tekstu w tym polu
		JFormattedTextField frmtdtxtfldXxxx = new JFormattedTextField();
		frmtdtxtfldXxxx.setText("xxxx");
		frmtdtxtfldXxxx.setBounds(43, 39, 90, 20);
		contentPane.add(frmtdtxtfldXxxx);	
		

		
		JButton btnConnect = new JButton("Connect");
		btnConnect.setBounds(10, 70, 90, 23);
		btnConnect.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e) 
			{ //Przycisk Connect tworzy nowy watek dla klienta
				new Thread() {
					public void run() {
						try {
							Client client = new Client(tempo.getText(),Integer.parseInt(frmtdtxtfldXxxx.getText())); // Dane sa pozyskiwane z JFormattedTextField
							lblNotConnected.setText("Connected!");
							btnConnect.setEnabled(false);
							client.Play();

							
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}.start();
			}
		});
		contentPane.add(btnConnect);
		

		
		JLabel lblPort = new JLabel("Port:");
		lblPort.setBounds(10, 42, 30, 14);
		contentPane.add(lblPort);
		
		panel = new OrderPane();
		panel.setBounds(145, 14, 450, 450);
		contentPane.add(panel);
		
		 lblNotConnected = new JLabel("Not Connected", SwingConstants.CENTER);
		lblNotConnected.setForeground(new Color(255, 255, 255));
		lblNotConnected.setBackground(new Color(128, 128, 128));
		lblNotConnected.setOpaque(true);
		lblNotConnected.setBounds(10, 141, 123, 23);
		contentPane.add(lblNotConnected);
		 lbltura = new JLabel("Tura przeciwnika", SwingConstants.CENTER);
			lbltura.setBounds(10, 117, 123, 23);
			lbltura.setForeground( Color.WHITE);
			lbltura.setBackground(Color.BLACK);
			lbltura.setOpaque(true);
			contentPane.add(lbltura);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new LineBorder(new Color(0, 0, 0), 3, true));
		panel_1.setBounds(10, 177, 123, 200);
		contentPane.add(panel_1);
		panel_1.setLayout(new GridLayout(5, 1, 5, 5));
		text= new JTextField("Letter") ; //pole w ktore wpisywane sa litery ktore moga sie znalexc w hasle
		panel_1.add(text);
		btnlit = new JButton("Send");// przycisk wysylajacy te litery
		btnlit.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				if(Client.tura == true)
				{
					if(text.getText().length()==1 &&ltr.getText().indexOf(text.getText().charAt(0))<0&&panel.Zlelitery.getText().indexOf(text.getText().charAt(0))<0)
					{// ten warunek sprawdza czy pobierana wartosc odpowiedzi jest pojedyncza litera i nie byla juz uzyta
					litera=text.getText().charAt(0);
					Client.wysl=text.getText(); //przypisanie tekstu do zmiennej wysylanej
					Sprawdzenie();// sprawdzanie czy ta litera miesci sie w hasle czy nie
					}
					else
					{
						JOptionPane.showMessageDialog(null, "Zla wartosc zostala wpisana!"); //
					}
				}
			}
		});
		panel_1.add(btnlit);
		textanswer= new JTextField("Word");// pole w ktore wpisywane sa potencjalne odpowiedzi
		panel_1.add(textanswer);
		btnhas = new JButton("Send"); //przycisk wysylajacy potencjalne odpowiedzi
		btnhas.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				if(Client.tura == true)
				{
					if(textanswer.getText().length()>1 &&panel.Zlehasla.getText().indexOf(textanswer.getText())<0)
					{//warunek sprawdza czy takie haslo nie bylo juz uzyte i czy jest dluzsze niz jedna litera
					haslo=textanswer.getText();
					Client.wysl=textanswer.getText();
					SprawdzenieH();	
					}
					else
					{
						JOptionPane.showMessageDialog(null, "Zla wartosc zostala wpisana!");
					}
				}
			}
		});
		panel_1.add(btnhas);
		panel_1.add(ltr);
		ltr.setFont(new Font("Century Gothic", Font.BOLD, 14 ));
	}
	private void Sprawdzenie()
	{
	    char[] temp = ltr.getText().toCharArray();    			
		for(int i=0; i<odp.length(); i++)
		{
			if(litera==odp.charAt(i))
			{
				temp[i]=litera;
				jest=true;
			}
		} //petla sprawdzajaca czy nowa litera znajduje sie w hasle. miejsca w labelu gdzie powinna ona sie znalexc zostaja nia zastapione
	    String temp2= new String(temp);
	    ltr.setText(temp2); //ustawianie nowego tekstu dla etykiety (badz takiego samego, jesli nic sie nie zmienilo)
	    if(jest==false)
	    {// jesli nowa litera nie znalazla sie w odpoiwedzi chociaz raz, to rysowana jest kolejna czesc wiseilca: mozna przegrac podajac zarowno zle hasla jak i litery
			panel.numer++;
			panel.repaint();
			panel.Zlelitery.setText(panel.Zlelitery.getText()+" "+litera); //wyswietlanie juz uzytych zlych liter
	    }
	    jest=false;
	    if(panel.numer==10||ltr.getText().compareTo(odp)==0)
	    {
			JOptionPane.showMessageDialog(null, "Remis"); //jesli zaden gracz nie poda prawidlowej odpowiedzi przed odslonieciem wszystkich liter badz wisielec zostanie narysowany w calosci to jest remis
			frame.dispose();
			System.exit(getDefaultCloseOperation());
	    }
	}
	public  void SprawdzeniePrzych() //identyczna funkcaj tylko dla wartosci otrzymywanych od 2 gracza
	{
	    char[] temp = ltr.getText().toCharArray();    			
		for(int i=0; i<odp.length(); i++)
		{
			if(literaPrzych==odp.charAt(i))
			{
				temp[i]=literaPrzych;
				jest=true;
			}
		}
	    String temp2= new String(temp);
	    ltr.setText(temp2);
	    if(jest==false)
	    {
			panel.numer++;
			panel.repaint();
			panel.Zlelitery.setText(panel.Zlelitery.getText()+" "+literaPrzych);
	    }
	    jest=false;
	    if(panel.numer==10||ltr.getText().compareTo(odp)==0)
	    {
			JOptionPane.showMessageDialog(null, "Remis");
			frame.dispose(); //jesli rozgrywka zostanie zakonczona to program jest wylaczany
			System.exit(getDefaultCloseOperation());
	    }
	}
	public  void SprawdzenieH() //funkcja sprawdzajaca czy opdowiedz podana przez gracza jest prawidlowa
	{

	    if(odp.compareTo(haslo)!=0)
	    {
			panel.numer++; // po tej zmiennej mozna rozpoznac ile wisielec juz ma czesci i je narysowac
			panel.repaint();// malowana jest kolejna czesc wisielca
			panel.Zlehasla.setText(panel.Zlehasla.getText()+System.lineSeparator()+haslo);
		    if(panel.numer==10||ltr.getText().compareTo(odp)==0)
		    {
				JOptionPane.showMessageDialog(null, "Remis");
				frame.dispose();
				System.exit(getDefaultCloseOperation());
		    }
	    }
	    else
	    {
	    	ltr.setText(haslo);
			JOptionPane.showMessageDialog(null, "Klient wygrana!");
			frame.dispose();
			System.exit(getDefaultCloseOperation());
	    }
	}
	public  void SprawdzenieHPrzyh() //funkcja sprawdzajaca czy opdowiedz podana przez 2 gracza jest prawidlowa
	{

	    if(odp.compareTo(hasloprzes)!=0)
	    {
			panel.numer++;
			panel.repaint();
			panel.Zlehasla.setText(panel.Zlehasla.getText()+System.lineSeparator()+hasloprzes);
		    if(panel.numer==10||ltr.getText().compareTo(odp)==0)
		    {
				JOptionPane.showMessageDialog(null, "Remis");
				frame.dispose();
				System.exit(getDefaultCloseOperation());

		    }
	    }
	    else
	    {
	    	ltr.setText(hasloprzes);
			JOptionPane.showMessageDialog(null, "Klient przegrana!");
			frame.dispose();
			System.exit(getDefaultCloseOperation());
	    }
	}
}